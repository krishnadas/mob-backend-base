'''Defines which serializers comprise the registration app.'''
from rest_framework import serializers
from .models import AppUser


class UserSerializer(serializers.ModelSerializer):
    """
    Used for Serializing the user object
    """
    class Meta:

        """ meta datas of model
        """
        model = AppUser
        fields = ('name', 'email', 'mobile')
