"""
To register django admin panel
"""
from django.contrib import admin
from .models import AppUser


class AppUserAdmin(admin.ModelAdmin):
    '''
    Admin for AppUser
    '''
    model = AppUser
    list_display = ('name',
                    'email',
                    'mobile',
                    'created_at',
                    'update_at')
    list_filter = ('created_at',
                   'update_at')
admin.site.register(AppUser, AppUserAdmin)
