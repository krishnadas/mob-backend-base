'''Defines which models for registration app.'''
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


class AppUserManager(BaseUserManager):

    """
    Application User Manager
    """

    def create_superuser(self, email, name, password, mobile):
        """
        Application User model
        """
        if not email:
            raise ValueError('User must have a valid username')

        user = self.model(
            email=email,
            name=name,
            mobile=mobile,
            is_admin=True)

        user.set_password(password)
        user.save(using=self._db)
        return user


class AppUser(AbstractBaseUser):

    """
    Application User model
    """
    name = models.CharField(verbose_name="name", max_length=50)
    mobile = models.CharField(verbose_name="mobile", max_length=13)
    email = models.EmailField(verbose_name="email", unique=True)
    is_admin = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)
    objects = AppUserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name', 'mobile']

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        return True

    def get_short_name(self):
        '''
        Method for getting Name
        '''
        return self.name

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.is_admin

    class Meta:

        '''model meta data'''
        verbose_name = "Application User"
        verbose_name_plural = "Application Users"

    def __unicode__(self):
        '''model __unicode__ data'''
        return self.name + " <" + self.email + ">"
