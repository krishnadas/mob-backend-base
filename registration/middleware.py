'''Defines which middlewares for registration app.'''
from django.core import signing
from django.contrib.sites.models import RequestSite
from django.template import loader
from django.core.mail import EmailMultiAlternatives
from django.utils.html import strip_tags
from django.conf import settings
from utils.threads import EmailThread
import logging


logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger('django')


def send_email_on_signup(request, user_data):
    '''
    Method for sending welcome email after sign up
    '''
    email_template_name = 'registration/welcome.html'
    context = {
        'site': RequestSite(request),
        'user': user_data,
        'token': signing.dumps(user_data.pk, salt='Welcome Diabeat'),
        'secure': request.is_secure(),
        'host': request.META.get('HTTP_HOST')
    }
    html_content = loader.render_to_string(email_template_name,
                                           context)
    body = strip_tags(html_content)
    subject = "Welcome to Our App"
    EmailThread(subject, body, settings.EMAIL_TO, html_content).start()


def send_email_on_recover_password(request, user_data, salt):
    '''
    Method for sending password reset mail
    '''
    email_template_name = 'registration/email.html'
    context = {
               'site': RequestSite(request),
               'user': user_data,
               'token': signing.dumps(user_data.pk, salt=salt),
               'secure': request.is_secure(),
              }
    html_content = loader.render_to_string(email_template_name,
                                           context)
    body = strip_tags(html_content)
    subject = "Password Reset Request"
    email = EmailMultiAlternatives(
                subject,
                body, 'App <care@app.com>', [
                    user_data.email])
    email.attach_alternative(html_content, "text/html")
    email.send()
