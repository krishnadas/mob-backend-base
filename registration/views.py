'''Defines which views for registration app.'''
from django.core import signing
from django.contrib.sites.models import RequestSite
from django.contrib.auth import logout
from django.shortcuts import render_to_response
from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from utils.response_builder import get_response_json, get_error_json
from utils.conver_handler import string_to_dict
from utils.oauth_handler import curl_data, refresh_token
from .serializer import UserSerializer
from .middleware import send_email_on_signup, send_email_on_recover_password
from .models import AppUser
from .forms import PasswordResetForm
import datetime
import os
import pytz
import logging

logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger('django')

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class UserRegister(APIView):

    """
    Register a user
    """

    def post(self, request):
        '''Defines post method in UserRegister view'''
        import pdb;pdb.set_trace()
        user_obj = None
        try:
            logger.info('Entered registration view')
            serializer = UserSerializer(data=request.DATA)
            if serializer.is_valid():
                logger.info('Saving the data using Register Serializer')
                user_obj = serializer.save()
                logger.info('Saving the password')
                user_obj.set_password(serializer.data['password'])
                user_obj.save()
                logger.info('Registration done. Getting access token')
                auth_data = curl_data(
                    user_obj.email,
                    serializer.data['password'], request)
                if 'error' not in auth_data:
                    logger.info('Got access token. Preparing response')
                    data = UserSerializer(user_obj).data
                    data['access_token'] = auth_data['access_token']
                    data['refresh_token'] = auth_data['refresh_token']
                    response_data = get_response_json(
                        uri=request._request.path,
                        lang=request.DATA['lang'],
                        region=request.DATA['region'],
                        created=True,
                        responsecode=201,
                        start=0,
                        count=0,
                        total=1,
                        data=data)
                    send_email_on_signup(request, user_obj)
                    logger.info('Returning the response.')
                    headers = {
                        'access_token': data['access_token'],
                        'refresh_token': data['refresh_token']}
                    return Response(
                        response_data,
                        status=status.HTTP_201_CREATED, headers=headers)
                user_obj.delete()
            logger.warning('Got invalid credentials. Returning error response')
            detail = serializer.errors
            response_data = get_error_json(uri=request._request.path,
                                           lang=request.DATA['lang'],
                                           region=request.DATA['region'],
                                           description='Invalid credentials',
                                           detail=detail,
                                           responsecode=400)
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)
        except Exception, e:
            logger.error(e)
            if user_obj:
                user_obj.delete()
            logger.error('Got exception. Returning Internal server error')
            response_data = get_error_json(uri=request._request.path,
                                           lang=request.DATA['lang'],
                                           region=request.DATA['region'],
                                           description='Internal server error',
                                           detail='Internal server error',
                                           responsecode=500)
            return Response(
                response_data,
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class UserLogin(APIView):

    """
    User login view
    """

    def post(self, request):
        '''Defines post method in UserLogin view'''
        logger.info('Entered Login view')
        email = request.DATA.get('email')
        password = request.DATA.get('password')
        auth_data = curl_data(email, password, request)
        logger.info(
            'authentication with email: ' + email + 'and pwd: ' + password)
        if 'error' not in auth_data:
            logger.info('authentication was successful.')
            user = AppUser.objects.get(email=email)
            logger.info('Got access token. Preparing response')
            data = UserSerializer(user).data
            data['access_token'] = auth_data['access_token']
            data['refresh_token'] = auth_data['refresh_token']
            response_data = get_response_json(
                uri=request._request.path,
                lang=request.DATA['lang'],
                region=request.DATA['region'],
                created=False,
                responsecode=200,
                start=0,
                count=0,
                total=1,
                data=data)
            logger.info('Sending the response')
            headers = {
                'access_token': data['access_token'],
                'refresh_token': data['refresh_token']}
            return Response(
                data=response_data, status=status.HTTP_200_OK, headers=headers)
        logger.info('authentication unsuccessful. Preparing response')
        response_data = get_error_json(uri=request._request.path,
                                       lang=request.DATA['lang'],
                                       region=request.DATA['region'],
                                       description='Invalid credentials',
                                       detail='Please check creds',
                                       responsecode=400)
        logger.info('Sending resposne')
        return Response(response_data, status=status.HTTP_400_BAD_REQUEST)


class SaltMixin(object):

    '''Make a salt object'''
    salt = 'password_recovery'
    url_salt = 'password_recovery_url'


class RecoverPassword(SaltMixin, APIView):

    """
    recover password of a user
    """

    def get(self, request):
        """
        For send email with password reset link to user
        """
        email = request.query_params.get("email")
        try:
            user_data = AppUser.objects.get(email=email)
            if user_data:
                send_email_on_recover_password(request, user_data, self.salt)
                response_data = get_response_json(
                    uri=request._request.path,
                    lang=request.query_params.get("lang"),
                    region=request.query_params.get("region"),
                    created=False,
                    updated=True,
                    responsecode=200,
                    start=0,
                    count=0,
                    total=1,
                    message="OK")
                return Response(data=response_data, status=status.HTTP_200_OK)

            response_data = get_error_json(
                uri=request._request.path,
                lang=request.query_params.get("lang"),
                region=request.query_params.get("region"),
                description='Bad request',
                detail="Bad request",
                responsecode=400,
            )
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)
        except:
            response_data = get_error_json(
                uri=request._request.path,
                lang=request.query_params.get("lang"),
                region=request.query_params.get("region"),
                description='Internal server error',
                detail='Internal server error',
                responsecode=500,
            )
            return Response(
                response_data,
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class RecoverPasswordMailView(SaltMixin, APIView):

    """
    recover password of a user
    """
    form = PasswordResetForm()
    token_expires = 3600 * 48

    def get(self, request, token):
        """
        For show the form to set new password if the token is valid
        """
        try:
            primary_key = signing.loads(
                token, max_age=self.token_expires, salt=self.salt)
            user = AppUser.objects.get(id=primary_key)
            if user:
                return render_to_response(
                                          'registration/email.html',
                                          {
                                           'in_browser': True,
                                           'site': RequestSite(request),
                                           'user': user,
                                           'token': signing.dumps(
                                                user.pk, salt=self.salt),
                                           'secure': request.is_secure(),
                                          })
        except signing.BadSignature:
            return render_to_response(
                'registration/show_message.html',
                {
                    'title': "invalid token",
                    'message': "sorry invalid token try again"})


class ResetPassword(SaltMixin, APIView):

    """
    recover password of a user
    """
    form = PasswordResetForm()
    token_expires = 3600 * 48

    def get(self, request, token):
        """
        For show the form to set new password if the token is valid
        """
        try:
            primary_key = signing.loads(
                token, max_age=self.token_expires, salt=self.salt)
        except signing.BadSignature:
            return render_to_response(
                'registration/show_message.html',
                {
                    'title': "invalid token",
                    'message': "sorry invalid token try again"})
        user = AppUser.objects.get(id=primary_key)
        return render_to_response(
            'registration/recovery_form.html',
            {'user': user, 'form': self.form})

    def post(self, request, token):
        """
        For set new password for the user
        """
        form = PasswordResetForm(request.DATA)
        if form.is_valid():
            primary_key = signing.loads(
                token, max_age=self.token_expires, salt=self.salt)
            user_data = AppUser.objects.get(id=primary_key)
            if user_data:
                user_data.set_password(request.DATA['password1'])
                user_data.save()
                return render_to_response(
                    'registration/show_message.html',
                    {
                        'title': "Change successfully",
                        'message': "your password has Change successfully"})
            return render_to_response(
                'registration/show_message.html',
                {
                    'title': "Sorry something wrong",
                    'message': "sorry try again to set new password"})
        return render_to_response(
            'registration/show_message.html',
            {
                'title': "Sorry something wrong",
                'message': "sorry try again to set new password"})


class Logout(APIView):

    '''logout user from request'''

    def get(self, request):
        '''Defines get method in Logout view'''
        date_now = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
        if request.auth is None or request.auth.expires < date_now:
            logger.warning(
                'Invalid access token. Returning unAuthorized response.')
            response_data = get_error_json(
                uri=request._request.path,
                lang=request.DATA['lang'],
                region=request.DATA['region'],
                description='unAuthorized',
                detail='unAuthorized',
                responsecode=401,
            )
            return Response(response_data, status=status.HTTP_401_UNAUTHORIZED)
        logout(request)
        return Response("logout successfully")


class RefreshToken(APIView):

    '''Get new access token by using refresh token'''

    def post(self, request):
        '''Defines post method in RefreshToken view'''
        refreshed_data = refresh_token(request, request.DATA['token'])
        if refreshed_data:
            data = {}
            auth_data = string_to_dict(refreshed_data.replace("\"", ""))
            data['access_token'] = auth_data['access_token']
            data['refresh_token'] = auth_data[' refresh_token']
            response_data = get_response_json(
                uri=request._request.path,
                lang=request.DATA['lang'],
                region=request.DATA['region'],
                created=False,
                responsecode=200,
                start=0,
                count=0,
                total=1,
                data=data)
            return Response(data=response_data, status=status.HTTP_200_OK)
        response_data = get_error_json(
            uri=request._request.path,
            lang=request.query_params.get("lang"),
            region=request.query_params.get("region"),
            description='Bad request',
            detail="Bad request",
            responsecode=400,
        )
        return Response(response_data, status=status.HTTP_400_BAD_REQUEST)
