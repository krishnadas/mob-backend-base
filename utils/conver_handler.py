''' module for covertor'''


def string_to_dict(string_input):
    """
    To conver string of list to list
    """
    if string_input is None or string_input == '':
        return ''
    data = string_input[1:-1].split(',')
    result = {}
    for data_value in data:
        result[data_value.split(':')[0]] = data_value.split(':')[1]
    return result


def list_to_string(list_input):
    """
    To conver string of list to list
    """
    if list_input is None or list_input == []:
        return '[]'
    result = str(','.join(list_input))
    return string_to_list(result)
