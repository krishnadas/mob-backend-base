""" Response Builder """
# python import
import json
# application import
from .response_codes import RESPONSE_MESSAGE, RESPONSE_CODE
from .response_codes import RESPONSE_MESSAGES_DICT
from .response_codes import ERROR_MESSAGE, ERROR_CODE


__author__ = 'Krishnadas'


class ResponseData(object):

    """The ResponseData object. It creates a response returned in json"""

    def __init__(self):
        self.uri = '/beingwellapi/1/access-token'
        self.lang = 'en-US'
        self.region = 'US'
        self.code = None

    def get_response_json(self, **kargs):
        """ get_response_json """
        for key, value in kargs.iteritems():
            setattr(self, key, value)
        self.message = RESPONSE_MESSAGE[self.code]
        self.responsecode = RESPONSE_CODE[self.code]
        delattr(self, "code")
        response_json = json.dumps(self.__dict__)

        return response_json

    def get_error_json(self, **kargs):
        """ get_error_json """
        for key, value in kargs.iteritems():
            setattr(self, key, value)
        self.description = ERROR_MESSAGE[self.code]
        self.responsecode = ERROR_CODE[self.code]
        delattr(self, "code")
        error_json = {'error': self.__dict__}
        response_json = json.dumps(error_json)

        return response_json


def get_response_json(**kargs):
    """
    This will create the success response for every services
    """
    response_list = (
        'uri',
        'lang',
        'region',
        'updated',
        'current_time',
        'created',
        'responsecode',
        'start',
        'count',
        'total',
        'data')
    response_dict = {
        'message': RESPONSE_MESSAGES_DICT.get(kargs['responsecode'])}
    for key, value in kargs.iteritems():
        if key in response_list:
            response_dict[key] = value

    return response_dict


def get_error_json(**kargs):
    """
    This will create the error response for every services
    """
    response_list = ('uri', 'lang', 'description', 'detail', 'responsecode')
    err_response_dict = {
        'description': RESPONSE_MESSAGES_DICT.get(kargs['responsecode'])}
    for key, value in kargs.iteritems():
        if key in response_list:
            err_response_dict[key] = value

    response_dict = {"error": err_response_dict}

    return response_dict


def get_req_meta_data(request):
    """
    This will gety the meta data for every services
    """
    uri = str(request.path)
    lang = str(request.META.get('HTTP_ACCEPT_LANGUAGE'))
    response = {'uri': uri, 'lang': lang}
    return response


def convert_string_json(string_data):
    """
    Convert the json response created back from string to json
    to log errors
    This is to avoid calling import json in all views
    """
    json_data = json.loads(string_data)
    return json_data
