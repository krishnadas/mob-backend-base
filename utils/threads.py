''' Threads for asyncrinous tasks '''
from django.core.mail import EmailMultiAlternatives
from django.conf import settings

import threading
import logging

__author__ = 'Krishnadas'

logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger('django')


class EmailThread(threading.Thread):
    ''' Thread for email sending '''
    def __init__(self, subject, body, email, html_content):
        ''' method for initialise Email Thread '''
        self.body = body
        self.email = email
        self.subject = subject
        self.html_content = html_content
        threading.Thread.__init__(self)

    def run(self):
        ''' method for run email thread '''
        logger.info('Preparing email.')
        email = EmailMultiAlternatives(
                self.subject,
                self.body, 'WellBing <care@wellbing.co>', [
                    self.email])
        email.attach_alternative(self.html_content, "text/html")
        email.send()
        logger.info('email send.')
