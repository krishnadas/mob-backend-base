''' Handle Oauth2 methods '''
from django.conf import settings
from oauth2_provider.models import AccessToken

import logging
import urllib2
import json
import pytz
import datetime

__author__ = 'Krishnadas'

logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger('django')


def curl_data(email, password, request):
    '''method to generate access token'''
    logger.info('Getting access token')
    logger.info(request.META['HTTP_HOST'])
    data = "client_id=" + settings.OAUTH_CLIENT_ID + \
           "&client_secret=" + settings.OAUTH_SECRET_KEY + \
           "&grant_type=password&username=" + email + \
           "&password=" + password + ""
    if request.is_secure():
        url = 'https://' + request.META['HTTP_HOST'] + '/o/token/'
    else:
        url = 'http://' + request.META['HTTP_HOST'] + '/o/token/'
    req = urllib2.Request(
        url, data, {
            'Content-Type': 'application/x-www-form-urlencoded'})
    try:
        url_data = urllib2.urlopen(req)
        result = url_data.read()
        url_data.close()
        access_token = AccessToken.objects.get(
            token=json.loads(result)['access_token'])
        access_token.expires = datetime.datetime.utcnow().replace(
            tzinfo=pytz.utc) + datetime.timedelta(days=200)
        access_token.scope += " offline"
        access_token.save()
        return json.loads(result)
    except Exception as e:
        logger.error(e.message)
        return {'error': 'Invalid creds'}


def refresh_token(request, token):
    '''method to get new access token from refresh token'''
    data = "refresh_token=" + token + \
        "&client_id=" + settings.OAUTH_CLIENT_ID + \
        "&client_secret=" + settings.OAUTH_SECRET_KEY + \
        "&grant_type=refresh_token"
    if request.is_secure():
        url = 'https://' + request.META['HTTP_HOST'] + '/o/token/'
    else:
        url = 'http://' + request.META['HTTP_HOST'] + '/o/token/'
    req = urllib2.Request(
        url, data, {
            'Content-Type': 'application/x-www-form-urlencoded'})
    try:
        pdf_file = urllib2.urlopen(req)
        result = pdf_file.read()
        pdf_file.close()
        return result
    except Exception as e:
        logger.error(e.message)
        return None


def valiate_access_token(user_id, data):
    '''
    Validating the AccessToken
    '''
    logger.info('Validating user by considering the user_d from url and user from request.auth')
    date_now = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
    if data and data.expires > date_now:
        if data.user.id == int(user_id):
            return data.user
        else:
            return False
    else:
        return {'error': True}