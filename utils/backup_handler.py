''' Data export methods '''
from django.template.loader import get_template
from django.template import Context
from django.conf import settings

import pdfkit
import xlsxwriter
import string
import logging

__author__ = 'Krishnadas'

logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger('django')


def pdf_export(template_src, context_dict):
    '''Method to cponvert html to pdf'''
    options = {
        'page-size': 'A4',
        'margin-top': '0',
        'margin-right': '0',
        'margin-bottom': '0',
        'margin-left': '0',
        'encoding': "UTF-8",
        'no-outline': None
    }
    template = get_template(template_src)
    context = Context(context_dict)
    html = template.render(context)
    pdf = pdfkit.from_string(html, False, options=options)
    return pdf


def exel_export(title, context_dict):
    alpha_list = list(string.ascii_uppercase)
    logger.info('Method for preparing excel sheet with user data.')
    workbook = xlsxwriter.Workbook('Backup_' + title + '.xlsx')
    logger.info('Create a workbook and add a worksheet.')
    worksheet = workbook.add_worksheet()
    # Add a number format for cells with money.
    logger.info('preparing cell formats for data report excel.')
    cell_format = workbook.add_format({
        'align': 'left',
        'valign': 'vcenter',
        'bold': True,
        'bg_color': '#92D050',
        'font_size': 10,
        'font_name': 'Calibri'})
    logger.info('Preparing header for the Excel sheet')
    worksheet.merge_range(
        'A1:' + alpha_list[len(context_dict)-1] + '1', "", cell_format)
    worksheet.write_rich_string('A1', title, cell_format)
    logger.info('Method for writing 4 column data in excel.')
    level = 2
    for key, value in context_dict.items():
        alpha_list[level]
        worksheet.write(alpha_list[level] + '2', key, cell_format)
        rows = 3
        for data in value:
            worksheet.write(alpha_list[level] + str(rows), data, cell_format)
            rows += 1
        level += 1
    workbook.close()
    return True
